# Kimun Openfaas

Openfaas makes it easy for developers to deploy event-driven functions and microservices to Kubernetes without repetitive, boiler-plate coding. Package your code or an existing binary in a Docker image to get a highly scalable endpoint with auto-

OpenFaaS is the only Serverless framework which puts containers in the spotlight and allows any code or binary for Linux or Windows to become a serverless function.

# Serverless vs FaaS  
Serverless and functions-as-a-Service are often conflated with one another but the truth is that FaaS is actually a subset of serverless.
Serverless is focused on any service category, be it compute, storage, database, etc. where configuration management , and billing of servers are invisible to the end user.
FaaS, on the other hand, while perhaps the most central technology in serverless architecture, is focused on the event-driven computing paradigm wherein application code, or containers, only run in response to events or requests.

## FaaS
* Functions as a Service are small pice of code (a fuction)
* Is focused on the event-driven computing.
* you don't need take care about the infrastructure
* you upload your code on the plataform then  you need associate that code to one trigger or event, for axample one http request
*  the code just run when is triggered, run the process, return something and then turn off, there is not VM to halt or containers to stop.
* this functions just do one thing
* Writing code, testing code is more easyer
* Small picess of code, triggered in one event couse, so just run when is required.

![OpenFaaS Life Cycle](img/openfaas-life-cycle.png "Life Cycle")

* you can save money with this, because you just pay when it's run.

## OpenFaaS Watchdog
It's a small webserver that runs the main process in the container.
Http request comming to the Watchdog webserver and this get forward to the fuction, that also exist in the container. (stdin / stdout)

## Prometheus
It's a cloud native monitor tool. This tool is monitoring OpenFaaS you can use the default alerts or create new ones and also see metrics of the functions among other things

## Hands-on with OpenFaaS
![OpenFaaS](img/openfaas.png)
1. Write your function
2. Package your function into one container, for example using a Dockerfile, and finally deploy that container.
![deploy flow](img/deploy-flow.png)

## Install Client
* [install faas-cli](https://docs.openfaas.com/cli/install/)

```bash
 curl -sSL https://cli.openfaas.com | sh
```

## Install
Here you can deploy your functions and see them and managed them
* [Function example](https://github.com/openfaas/faas/tree/master/sample-functions/figletf)
* [Video Serverless | Install OpenFaas on Docker Swarm](https://youtu.be/tfjArV6G040)
```bash
git clone https://github.com/openfaas/faas.git
cd faas
git checkout 0.20.5
./deploy_stack.sh
rometheus
docker stack ls
```
* openfaas url: http://localhost:8080/ui/
* prometheus url: http://localhost:9090/graph

If you lose the password you can recover the password with [this instructions](https://docs.openfaas.com/deployment/troubleshooting/#i-forgot-my-gateway-password)

> Before  to deploy some application first you need  execute  a `faas-cli login`
```bash
$ docker service rm print-password \
 ; docker service create --detach --secret basic-auth-password \
   --restart-condition=none --name=print-password \
   alpine:3.9 cat /run/secrets/basic-auth-password

$ docker service logs print-password
print-password.1.59bwe0bb4d99@nuc    | 21f596c9cd75a0fe5e335fb743995d18399e83418a37a79e719576a724efbbb6
```

# kubectl commands
* `kubectl get deploy -n openfaas` - list OpenFaaS microservices
* `kubectl get deploy -n openfaas-fn`


# faas-cli commands
* `faas-cli version` - get version
* `echo -n <fake-password> | faas-cli login --username=admin --password-stdin` - login
* `faas-cli template store list` - list of functions
* `faas-cli template pull` - pull template to build a img
* `faas-cli template pull https://github.com/andreipope/my-custom-store/` - pull a custom template
* `faas-cli new appfleet-hello-world --lang node`

# Links
* [Introduction to Serverless](https://www.udemy.com/course/introduction-to-serverless
* [Serverless](https://www.ibm.com/cloud/learn/serverless)
* [OpenFaaS Serverless code](https://github.com/openfaas/faas/tree/0.20.5)
* [OpenFaaS Install in miniKube](https://medium.com/@JockDaRock/openfaas-on-minikube-serverless-on-kubernetes-for-windows-10-3c7c7d68f47f)
