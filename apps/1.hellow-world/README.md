# Create serverless function with OpenFaaS
* [source](https://appfleet.com/blog/create-serverless-functions-with-openfaas/)

## Create a Serverless Function Using the CLI

Now that OpenFaaS and the faas-cli command-line utility are installed, you can create and deploy serverless functions using the built-in template engine. OpenFaaS provides two types of templates:

* The Classic templates are based on the Classic Watchdog and use stdio to communicate with your serverless function. Refer to the Watchdog page for more details about how OpenFaaS Watchdog works.

* The of-watchdog templates use HTTP to communicate with your serverless function. These templates are available through the [OpenFaaS Incubator](https://github.com/openfaas-incubator) GitHub repository.

In this tutorial, you'll use a classic template.

1. Run the following command to see the templates available in the official store:
```bash
faas-cli template store list
```
*  Note that you can specify an alternative store for templates. The following example command lists the templates from a repository named andreipope:
```bash
faas-cli template store list -u https://raw.githubusercontent.com/andreipope/my-custom-store/master/templates.json
```

2.  Download the official templates locally:
```bash
faas-cli template pull
```

```bash
Fetch templates from repository: https://github.com/openfaas/templates.git at master
2020/03/11 20:51:22 Attempting to expand templates from https://github.com/openfaas/templates.git
2020/03/11 20:51:25 Fetched 19 template(s) : [csharp csharp-armhf dockerfile go go-armhf java11 java11-vert-x java8 node node-arm64 node-armhf node12 php7 python python-armhf python3 python3-armhf python3-debian ruby] from https://github.com/openfaas/templates.git
```
*  By default, the above command downloads the templates from the OpenFaaS official GitHub repository. If you want to use a custom repository, then you should specify the URL of your repository. The following example command pulls the templates from a repository named andreipope:

```bash
faas-cli template pull https://github.com/andreipope/my-custom-store/
```

3. To create a new serverless function, run the faas-cli new command specifying:
* The name of your new function (appfleet-hello-world)
* The lang parameter followed by the programming language template (node).
```bash
faas-cli new appfleet-hello-world --lang node
```

output

```bash
Folder: appfleet-hello-world created.
  ___                   _____           ____
 / _ \ _ __   ___ _ __ |  ___|_ _  __ _/ ___|
| | | | '_ \ / _ \ '_ \| |_ / _` |/ _` \___ \
| |_| | |_) |  __/ | | |  _| (_| | (_| |___) |
 \___/| .__/ \___|_| |_|_|  \__,_|\__,_|____/
      |_|


Function created in folder: appfleet-hello-world
Stack file written: appfleet-hello-world.yml

Notes:
You have created a new function which uses Node.js 12.13.0 and the OpenFaaS
Classic Watchdog.

npm i --save can be used to add third-party packages like request or cheerio
npm documentation: https://docs.npmjs.com/

For high-throughput services, we recommend you use the node12 template which
uses a different version of the OpenFaaS watchdog.
```
At this point, your directory structure should look like the following:

```bash
tree . -L 2
```
```bash
.
├── appfleet-hello-world
│   ├── handler.js
│   └── package.json
├── appfleet-hello-world.yml
└── template
    ├── csharp
    ├── csharp-armhf
    ├── dockerfile
    ├── go
    ├── go-armhf
    ├── java11
    ├── java11-vert-x
    ├── java8
    ├── node
    ├── node-arm64
    ├── node-armhf
    ├── node12
    ├── php7
    ├── python
    ├── python-armhf
    ├── python3
    ├── python3-armhf
    ├── python3-debian
    └── ruby

21 directories, 3 files
```

Things to note:

* The appfleet-hello-world/handler.js file contains the code of your serverless function. You can use the echo command to list the contents of this file:

```bash
cat appfleet-hello-world/handler.js
```

```bash
"use strict"

module.exports = async (context, callback) => {
    return {status: "done"}
}
```

You can specify the dependencies required by your serverless function in the package.json file. The automatically generated file is just an empty shell:
```bash
cat appfleet-hello-world/package.json
```
```bash
{
  "name": "function",
  "version": "1.0.0",
  "description": "",
  "main": "handler.js",
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1"
  },
  "keywords": [],
  "author": "",
  "license": "ISC"
}
```
* The spec of the appfleet-hello-world function is stored in the appfleet-hello-world.yml file:

```bash
cat appfleet-hello-world.yml
```
```bash
version: 1.0
provider:
  name: openfaas
  gateway: http://127.0.0.1:8080
functions:
  appfleet-hello-world:
    lang: node
    handler: ./appfleet-hello-world
    image: appfleet-hello-world:latest
```

# Build Your Serverless Function
1. Open the appfleet-hello-world.yml file in a plain-text editor, and update the image field by prepending your Docker Hub user name to it. The following example prepends my username (andrepopescu12) to the image field:

```bash
image: andrepopescu12/appfleet-hello-world:latest
```
Once you've made this change, the `appfleet-hello-world.yml` file should look similar to the following:

```bash
version: 1.0
provider:
  name: openfaas
  gateway: http://127.0.0.1:8080
functions:
  appfleet-hello-world:
    lang: node
    handler: ./appfleet-hello-world
    image: <YOUR-DOCKER-HUB-ACCOUNT>/appfleet-hello-world:latest
```
2. Build the function. Enter the `faas-cli build` command specifying the `-f` argument with the name of the YAML file you edited in the previous step (`appfleet-hello-world.yml`):

```bash
faas-cli build -f appfleet-hello-world.yml
```
output:

```bash
[0] > Building appfleet-hello-world.
Clearing temporary build folder: ./build/appfleet-hello-world/
Preparing: ./appfleet-hello-world/ build/appfleet-hello-world/function
Building: andreipopescu12/appfleet-hello-world:latest with node template. Please wait..
Sending build context to Docker daemon  10.24kB
Step 1/24 : FROM openfaas/classic-watchdog:0.18.1 as watchdog
 ---> 94b5e0bef891
Step 2/24 : FROM node:12.13.0-alpine as ship
 ---> 69c8cc9212ec
Step 3/24 : COPY --from=watchdog /fwatchdog /usr/bin/fwatchdog
 ---> Using cache
 ---> ebab4b723c16
Step 4/24 : RUN chmod +x /usr/bin/fwatchdog
 ---> Using cache
 ---> 7952724b5872
Step 5/24 : RUN addgroup -S app && adduser app -S -G app
 ---> Using cache
 ---> 33c7f04595d2
Step 6/24 : WORKDIR /root/
 ---> Using cache
 ---> 77b9dee16c79
Step 7/24 : ENV NPM_CONFIG_LOGLEVEL warn
 ---> Using cache
 ---> a3d3c0bb4480
Step 8/24 : RUN mkdir -p /home/app
 ---> Using cache
 ---> 65457e03fcb1
Step 9/24 : WORKDIR /home/app
 ---> Using cache
 ---> 50ab672e5660
Step 10/24 : COPY package.json ./
 ---> Using cache
 ---> 6143e79de873
Step 11/24 : RUN npm i --production
 ---> Using cache
 ---> a41566487c6e
Step 12/24 : COPY index.js ./
 ---> Using cache
 ---> 566633e78d2c
Step 13/24 : WORKDIR /home/app/function
 ---> Using cache
 ---> 04c9de75f170
Step 14/24 : COPY function/*.json ./
 ---> Using cache
 ---> 85cf909b646a
Step 15/24 : RUN npm i --production || :
 ---> Using cache
 ---> c088cbcad583
Step 16/24 : COPY --chown=app:app function/ .
 ---> Using cache
 ---> 192db89e5941
Step 17/24 : WORKDIR /home/app/
 ---> Using cache
 ---> ee2b7d7e8bd4
Step 18/24 : RUN chmod +rx -R ./function     && chown app:app -R /home/app     && chmod 777 /tmp
 ---> Using cache
 ---> 81831389293e
Step 19/24 : USER app
 ---> Using cache
 ---> ca0cade453f5
Step 20/24 : ENV cgi_headers="true"
 ---> Using cache
 ---> afe8d7413349
Step 21/24 : ENV fprocess="node index.js"
 ---> Using cache
 ---> 5471cfe85461
Step 22/24 : EXPOSE 8080
 ---> Using cache
 ---> caaa8ae11dc7
Step 23/24 : HEALTHCHECK --interval=3s CMD [ -e /tmp/.lock ] || exit 1
 ---> Using cache
 ---> 881b4d2adb92
Step 24/24 : CMD ["fwatchdog"]
 ---> Using cache
 ---> 82b586f039df
Successfully built 82b586f039df
Successfully tagged andreipopescu12/appfleet-hello-world:latest
Image: andreipopescu12/appfleet-hello-world:latest built.
[0] < Building appfleet-hello-world done in 2.25s.
[0] Worker done.

Total build time: 2.25s
```

3. You can list your Docker images with:
```
docker images
```

# Deploy Your Function Using the CLI

1. Run the faas-cli deploy command to deploy your serverless function:
```bash
faas-cli deploy -f appfleet-hello-world.yml
```
output

```bash
Deploying: appfleet-hello-world.
WARNING! Communication is not secure, please consider using HTTPS. Letsencrypt.org offers free SSL/TLS certificates.
Handling connection for 8080
Handling connection for 8080

Deployed. 202 Accepted.
URL: http://127.0.0.1:8080/function/appfleet-hello-world
```

2. Use the faas-cli list command to list the functions deployed to your local OpenFaaS gateway:

```bash
faas-cli list
```

```bash
Function                      	Invocations    	Replicas
appfleet-hello-world          	0              	1
```

Note that you can also list the functions deployed to a different gateway by providing the URL of the gateway as follows:

```bash
faas-cli list --gateway https://<YOUR-GATEWAT-URL>:<YOUR-GATEWAY-PORT>
```

4. You can use the `faas-cli` describe method to retrieve more details about the `appfleet-hello-world` function:
```bash
faas-cli describe appfleet-hello-world
```

```bash
Name:                appfleet-hello-world
Status:              Ready
Replicas:            1
Available replicas:  1
Invocations:         1
Image:               andreipopescu12/appfleet-hello-world:latest
Function process:    node index.js
URL:                 http://127.0.0.1:8080/function/appfleet-hello-world
Async URL:           http://127.0.0.1:8080/async-function/appfleet-hello-world
Labels:              faas_function : appfleet-hello-world
Annotations:         prometheus.io.scrape : false
```

# Links
* [Create serverless functions with OpenFaaS](https://appfleet.com/blog/create-serverless-functions-with-openfaas/)
