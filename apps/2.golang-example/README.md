# Build a Serverless Golang Function  with OpenFaaS

# Pre-reqs

    * Go 1.8.3 or later (we use this on the host system to vendor packages)
    * Docker
    * OpenFaaS deployed (locally or remotely)
    * OpenFaaS CLI (installed in this tutorial)

This guide assumes you have already deployed OpenFaaS on your laptop or the cloud.

# Starting an App
* Create a function from a template
Use `faas-cli new` to create a template in one of the supported languages, or `faas-cli new --lang Dockerfile` to use a custom Dockerfile.

* Then use the CLI to create your Go function:
```bash
faas-cli new --lang go gohash
```

output:
```bash
2021/02/02 10:45:52 No templates found in current directory.
2021/02/02 10:45:52 Attempting to expand templates from https://github.com/openfaas/templates.git
2021/02/02 10:45:54 Fetched 12 template(s) : [csharp dockerfile go java11 java11-vert-x node node12 php7 python python3 python3-debian ruby] from https://github.com/openfaas/templates.git
Folder: gohash created.
  ___                   _____           ____
 / _ \ _ __   ___ _ __ |  ___|_ _  __ _/ ___|
| | | | '_ \ / _ \ '_ \| |_ / _` |/ _` \___ \
| |_| | |_) |  __/ | | |  _| (_| | (_| |___) |
 \___/| .__/ \___|_| |_|_|  \__,_|\__,_|____/
      |_|


Function created in folder: gohash
Stack file written: gohash.yml

Notes:
You have created a new function which uses Golang 1.13 and the Classic
OpenFaaS template.

To include third-party dependencies, use Go modules and use
"--build-arg GO111MODULE=on" with faas-cli build or configure this  
via your stack.yml file.

See more: https://docs.openfaas.com/cli/templates/

For high-throughput applications, we recommend using the golang-http
or golang-middleware templates instead available via the store.
```

We now have a folder called gohash with a handler.go file inside:

```
import (
	"fmt"
)

// Handle a serverless request
func Handle(req []byte) string {
	return fmt.Sprintf("Hello, Go. You said: %s", string(req))
}
```

Now you build / deploy and invoke the function.

```bash
faas-cli build -f gohash.yml
```

Now let's deploy the function:

```bash
faas-cli deploy -f gohash.yml
```
output:
```
Deploying: gohash.
WARNING! Communication is not secure, please consider using HTTPS. Letsencrypt.org offers free SSL/TLS certificates.

Deployed. 202 Accepted.
URL: http://127.0.0.1:8080/function/gohash.openfaas-fn
```

* NOTE: as of 0.7.6 of the OpenFaaS CLI you can replace faas-cli build/push/deploy with a single command: `faas-cli up`.

If this is on a remote cluster add the parameter --gateway http://...

Finally you can invoke your function using the OpenFaaS UI or the CLI with:

```bash
echo -n "test" | faas-cli invoke gohash
```
output:
```
Hello, Go. You said: test
```

If you'd like to delete the function run this:

```
echo -n "test" | faas-cli delete gohash
```



# Links
* [Serverless Golang with openfaas](https://blog.alexellis.io/serverless-golang-with-openfaas/)
800360132
