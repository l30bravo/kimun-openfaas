# Build a Serverless Golang Function  with OpenFaaS 3rd party dependency
Quick example of a project called structhash which generates a hash in a string format of arbitrary structs. This is useful for creating checksums and hashes of graphs within memory.


Structhash is a Go library for generating hash strings of arbitrary data structures.
This is a snippet from the structhash QuickStart on GitHub

1. `faas-cli new --lang go gohash`
2. edit handler.go file
```
package main

import (
    "fmt"
    "crypto/md5"
    "crypto/sha1"
    "github.com/cnf/structhash"
)

type S struct {
    Str string
    Num int
}

func main() {
    s := S{"hello", 123}

    hash, err := structhash.Hash(s, 1)
    if err != nil {
        panic(err)
    }
    fmt.Println(hash)
    // Prints: v1_41011bfa1a996db6d0b1075981f5aa8f
}
```

3. Build a custom function.
Let's take a quick look at the signature of our Go template:
```
func Handle(req []byte) string {
	return fmt.Sprintf("Hello, Go. You said: %s", string(req))
}
```
You can see the input is of type []byte which means you can also deal with binary data, or just convert this to a string as we are doing above.

4. Vendor the library
Let's go ahead and vendor the dependency of `github.com/cnf/structhash` so we can use it in our function.
