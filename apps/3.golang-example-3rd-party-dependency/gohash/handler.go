package main

import (
	"crypto/md5"
	"crypto/sha1"
	"fmt"
	"github.com/cnf/structhash"
)

type S struct {
	Str string
	Num int
}

func main() {
	s := S{"hello", 123}
	hash, err := structhash.Hash(s, 1)
	if err != nil {
		panic(err)
	}
	fmt.Println(hash)
	// Prints: v1_41011bfa1a996db6d0b1075981f5aa8f
}
